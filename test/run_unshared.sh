#!/bin/zsh
#

MOUNT=$(buildah mount hectorwork)

mount --bind "${0:a:h}/../.." ${MOUNT}/wdir
mount --bind "$(pwd)" ${MOUNT}/wtree

buildah run --workingdir /wtree hectorwork ${@}
