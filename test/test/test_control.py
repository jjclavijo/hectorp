import pytest
import tempfile

import sys
import io
import os

from hectorp.control import Control

def test_non_exient_file():
    with pytest.raises(FileNotFoundError):
        control = Control('nonexistent_path1',path_provided=True)

def test_horphan_label():
    with pytest.raises(ValueError):
        with tempfile.NamedTemporaryFile() as f:
            with open(f.name,'w') as fo:
                print('labela 0.2',
                      'labelb 1',
                      'labelc',
                      sep='\n',
                      file=fo)

            control = Control(fo.name,path_provided=True)

def test_create_file():
    with pytest.raises(FileNotFoundError):
        control = Control('nonexistent_path2',
                            defaults={'a':'b'},
                            path_provided=False,
                            unatended=True)
    assert os.path.exists('nonexistent_path2')
    os.remove('nonexistent_path2')

def test_create_file_user(monkeypatch):
    monkeypatch.setattr('sys.stdin', io.StringIO('Y\n'))

    with pytest.raises(FileNotFoundError):
        control = Control('nonexistent_path3',
                           defaults={'a':'b'},
                           path_provided=False,
                           unatended=False)

    assert os.path.exists('nonexistent_path3')
    os.remove('nonexistent_path3')

def test_no_create_file_user(monkeypatch):
    monkeypatch.setattr('sys.stdin', io.StringIO('N\n'))

    with pytest.raises(FileNotFoundError):
        control = Control('nonexistent_path4',
                            defaults={'a':'b'},
                            path_provided=False,
                            unatended=False)

    assert not os.path.exists('nonexistent_path4')



def test_types():
    with tempfile.NamedTemporaryFile() as f:
        with open(f.name,'w') as fo:
            print('labela 0.2',
                  'labelb 1',
                  'labelc Yes',
                  sep='\n',
                  file=fo)

        control = Control(fo.name,path_provided=True)
    assert isinstance(control.params['labela'],float)
    assert isinstance(control.params['labelb'],int)
    assert isinstance(control.params['labelc'],bool)
