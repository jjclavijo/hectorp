#!/usr/bin/env python3

from hectorp.simulatenoise import *

def main():

    params = {
            "SimulationDir":"dir",
            "SimulationLabel":"test_dict",
            "NumberOfSimulations":1,
            "NumberOfPoints":365,
            "SamplingPeriod":1,
            "TimeNoiseStart":0,
            "NoiseModels":["Matern", "GGM"],
            "Lambda":[0.1,0],
            "Kappa":[-0.75,0.28],
            "Sigma":[0.1,0.2],
            "GGM_1mphi":[0,1e-5],
            "RepeatableNoise":True,
            "MissingData":False,
            "PercMissingData":0.0,
            "Offsets":False,
            "Trend":0.2,
            "NominalBias":0.01,
            "AnnualSignal":0.5
            }

    control = simulatenoiseControl(params)

    observations = Observations()

    simulate_noise(control,observations)
    return 0

if __name__ == '__main__':
    exit(main())
