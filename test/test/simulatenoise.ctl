SimulationDir       dir
SimulationLabel     test_
NumberOfSimulations 1
NumberOfPoints      365
SamplingPeriod      1
TimeNoiseStart      0
NoiseModels         Matern GGM
Lambda              0.1 0
Kappa               -0.75 0.28
RepeatableNoise     No
MissingData         No
PercMissingData     0.0
Offsets             No
Trend               0
NominalBias         0
AnnualSignal        0
