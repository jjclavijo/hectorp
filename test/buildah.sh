#!/bin/zsh

if buildah inspect hectorwork 2>/dev/null 1>/dev/null
then
  echo Container Exists...
else

  CONT=$(buildah from --name hectorwork docker.io/python:3.9-bullseye)

  buildah run $CONT pip install --no-cache-dir --upgrade pip 
  buildah run $CONT pip install --no-cache-dir ipython

  MOUNT=$(buildah mount $CONT)

  mkdir -p ${MOUNT}/wdir

  mount --bind ../../ ${MOUNT}/wdir/

  buildah run --workingdir /wdir/hectorp $CONT pip install --no-cache-dir -e ./

  buildah umount ${CONT}
fi
