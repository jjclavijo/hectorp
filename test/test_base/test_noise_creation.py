#!/usr/bin/env python3

import io

from unittest import mock
import pytest

import logging

LOGGER = logging.getLogger(__name__)

import importlib.util
import sys

import numpy as np # to avoid reloading each time
import pandas as pd

N = 10

# --- Manual Load Hector Modules ---

my_specs = {}
my_modules = {}

h_spec = importlib.util.spec_from_file_location("hectorp",
                                              "/wtree/hectorpold/src/hectorp/__init__.py")
h_module = importlib.util.module_from_spec(h_spec)
my_modules["hectorp"] = h_module
my_specs["hectorp"] = h_spec

hc_spec = importlib.util.spec_from_file_location("hectorp.control",
                                              "/wtree/hectorpold/src/hectorp/control.py")
hc_module = importlib.util.module_from_spec(hc_spec)
my_modules["hectorp.control"] = hc_module
my_specs["hectorp.control"] = hc_spec

ho_spec = importlib.util.spec_from_file_location("hectorp.observations",
                                              "/wtree/hectorpold/src/hectorp/observations.py")
ho_module = importlib.util.module_from_spec(ho_spec)
my_modules["hectorp.observations"] = ho_module
my_specs["hectorp.observations"] = ho_spec

hs_spec = importlib.util.spec_from_file_location("hectorp.simulatenoise",
                                              "/wtree/hectorpold/src/hectorp/simulatenoise.py")
hs_module = importlib.util.module_from_spec(hs_spec)
my_modules["hectorp.simulatenoise"] = hs_module
my_specs["hectorp.simulatenoise"] = hs_spec

# ---------------------------------

def call_stable_main(options,stdinput,stdinput_call,writer=None):

    stdinput_call(stdinput)

    # --- prepare mocking of Control object with options dictionary
    mockControlParamsGetitem = mock.Mock(side_effect = options.__getitem__)
    mockControlParams = mock.MagicMock(**{"params.__getitem__":mockControlParamsGetitem})
    mockControl = mock.Mock(return_value = mockControlParams)

    # --- Mock modules to load the stable version of hectorp from gitlab --
    # https://stackoverflow.com/a/23992082/9296057
    with mock.patch.dict("sys.modules",**my_modules):

        # --- Execute modules and bind submodules ---
        my_specs['hectorp.control'].loader.exec_module(my_modules['hectorp.control'])
        my_specs['hectorp'].loader.exec_module(my_modules['hectorp'])
        my_modules['hectorp'].control = my_modules['hectorp.control']

        # --- Mock control ---
        with mock.patch('hectorp.control.Control',mockControl):

            # --- Execute modules and bind submodules ---
            # the modules executed here binds the mocked Control instance

            my_specs['hectorp.observations'].loader.exec_module(my_modules['hectorp.observations'])
            my_specs['hectorp.simulatenoise'].loader.exec_module(my_modules['hectorp.simulatenoise'])
            my_modules['hectorp'].observations = my_modules['hectorp.observations']
            my_modules['hectorp'].simulatenoise = my_modules['hectorp.simulatenoise']

            # --- Mock file writing for recording output without
            # accessing the filesystem

            # If no writer provided create own.
            writer_own = False
            if writer is None:
                writer = io.StringIO()
                writer_own = True

            # calling the mock returns itself, entering context returns the
            # same mock, not a copy, writing methods calls the StringIO
            # writing methods. "with" clauses don't close the StringIO.
            mockFile = mock.MagicMock(__call__=lambda *x,**y: x[0],
                                      __enter__=lambda x:x,
                                      write=writer.write,
                                      writelines=writer.writelines)

            # We will mock sys.open to always return the writer mock.
            mockOpen = mock.Mock(return_value=mockFile)


            mockMdir = mock.Mock()

            # Mock Filesistem Calls on observations, force CLI arguments to be
            # blank, Mock os.makedirs to do nothing.
            with mock.patch('hectorp.observations.open',mockOpen), \
                 mock.patch('sys.argv',['simulatenoise']), \
                 mock.patch('os.makedirs',mockMdir):

                hs_module.main()

        if writer_own: writer.seek(0) # if writer is a StringIO owned
                                      # by the function, rewind.

        return writer, { 'Control.params.__getitem__':mockControlParamsGetitem,
                         'Control.params':mockControlParams,
                         'Control':mockControl,
                         'output_file_write':mockFile,
                         'observations.open':mockOpen,
                         'os.makedirs':mockMdir}

testdataGGM = [ (10,1,0.23,0.01,1), (20,2.3,0.51,0.01,1) ]
@pytest.mark.parametrize("m,sigma,kappa,one_minus_phi,dt",testdataGGM)
def test_h_noise_generation_GGM(monkeypatch,m,sigma,kappa,one_minus_phi,dt):

    LOGGER.info(monkeypatch)

    options = {
               'SimulationDir':'None',
               "SimulationLabel":'None',
               "NumberOfSimulations":1,
               "NumberOfPoints":m,
               "SamplingPeriod":dt,
               "TimeNoiseStart":0,
               "GGM_1mphi":one_minus_phi,
               'NoiseModels': ["GGM"]
                }

    # Kappa, Sigma
    monkeypatch.setattr('sys.stdin',
                        io.StringIO(f'{kappa:.3f}\n{sigma:.3f}\n'))

    mparams = mock.Mock(side_effect = options.__getitem__)
    mm = mock.MagicMock(**{"params.__getitem__":mparams})
    cm = mock.Mock(return_value = mm)
    mdir = mock.Mock()

    # https://stackoverflow.com/a/23992082/9296057
    with mock.patch.dict("sys.modules",**my_modules):

        my_specs['hectorp.control'].loader.exec_module(my_modules['hectorp.control'])
        my_specs['hectorp'].loader.exec_module(my_modules['hectorp'])
        my_modules['hectorp'].control = my_modules['hectorp.control']

        with mock.patch('hectorp.control.Control',cm):

            my_specs['hectorp.observations'].loader.exec_module(my_modules['hectorp.observations'])
            my_specs['hectorp.simulatenoise'].loader.exec_module(my_modules['hectorp.simulatenoise'])
            my_modules['hectorp'].observations = my_modules['hectorp.observations']
            my_modules['hectorp'].simulatenoise = my_modules['hectorp.simulatenoise']

            myio = io.StringIO()

            #openmock = mock.MagicMock(__call__=lambda x: x,__enter__=lambda x:mockFile)
            mockFile = mock.MagicMock(__call__=lambda *x,**y: x[0],__enter__=lambda x:x, write=myio.write, writelines=myio.writelines)
            mockOpen = mock.Mock(return_value=mockFile)

            # Mock Filesistem Calls and CLI input.
            with mock.patch('hectorp.observations.open',mockOpen), \
                 mock.patch('sys.argv',['simulatenoise']), \
                 mock.patch('os.makedirs',mdir):

                hs_module.main()

        mockOpen.assert_called()

        #myio.seek(0)
        #noise = pd.read_fwf(myio,comment='#',names=['noise'],index_col=0)

        #LOGGER.info(noise)

    return None

import hectorpp.simulatenoise as hps

def test_new_create_noise():
    options = {
               'SimulationDir':'None',
               "SimulationLabel":'None',
               "NumberOfSimulations":1,
               "NumberOfPoints":100,
               "SamplingPeriod":1,
               "TimeNoiseStart":0,
               "GGM_1mphi":0.01,
               'NoiseModels': ["GGM"],
               "Kappa": [0.1],
               "Sigma": [0.2]
                }

    control = hps.simulatenoiseControl(options)
    y = hps.create_noise_(control)

    assert len(y) == 100

m = np.random.randint(10,1000,N)
sigma = np.random.uniform(0,10,N)
kappa = np.random.uniform(-2,2,N)
one_minus_phi = np.random.uniform(1e-6,1,N)
dt = np.random.uniform(0.5,20,N)
testdataGGM = zip(*map(lambda x: np.round(x,3),
                              (m,sigma,kappa,one_minus_phi,dt)))
#testdataGGM = [ (20,1,0.23,0.01,1), (30,2.3,0.51,0.01,1) ]
@pytest.mark.parametrize("m,sigma,kappa,one_minus_phi,dt",testdataGGM)
def test_noise_compare_GGM(monkeypatch,m,sigma,kappa,one_minus_phi,dt):

    # Create Reference noise
    options = {
               'SimulationDir':'None',
               "SimulationLabel":'None',
               "NumberOfSimulations":1,
               "NumberOfPoints":m,
               "SamplingPeriod":dt,
               "TimeNoiseStart":0,
               "GGM_1mphi":one_minus_phi,
               'NoiseModels': ["GGM"],
               'RepeatableNoise': True
                }

    inputs = [kappa,sigma]
    inputs = '\n'.join(map('{:.3f}'.format,inputs)) + '\n'
    # Kappa, Sigma

    stdin_call = lambda x: monkeypatch.setattr('sys.stdin',
                                               io.StringIO(x))

    myio, mocks = call_stable_main(options,inputs,stdin_call)

    noise = pd.read_fwf(myio,comment='#',names=['noise'],index_col=0)
    myio.close()

    # --- Real test starts here ---

    options = {
               'SimulationDir':'None',
               "SimulationLabel":'None',
               "NumberOfSimulations":1,
               "NumberOfPoints":m,
               "SamplingPeriod":dt,
               "TimeNoiseStart":0,
               "TS_format":'mom',
               "GGM_1mphi":one_minus_phi,
               'NoiseModels': ["GGM"],
               "Kappa": [kappa],
               "Sigma": [sigma]
                }

    mparams = mock.Mock(side_effect = options.__getitem__)
    mm = mock.MagicMock(**{"params.__getitem__":mparams,"__getitem__":mparams,"get":mparams})
    cm = mock.Mock(return_value = mm)
    mdir = mock.Mock()

    with mock.patch('hectorp.simulatenoise.Control',cm):
        control = hps.simulatenoiseControl(options,)

        y = hps.create_noise_(control,rng=np.random.default_rng(0))

    # LOGGER.info(f"nuevo: {len(y)}, viejo: {len(noise)}")
    # LOGGER.info(cm.mock_calls)
    # LOGGER.info(mparams.mock_calls)

    differ = (y - noise.noise.values)
    assert all(differ < 1e-5)

    return None

testdataGGM = zip(*map(lambda x: np.round(x,3),
                              (m,sigma,kappa,one_minus_phi,dt)))
#testdataGGM = [ (20,1,0.23,0.01,1), (30,2.3,0.51,0.01,1) ]
@pytest.mark.parametrize("m,sigma,kappa,one_minus_phi,dt",testdataGGM)
def test_noise_compare_GGM2(monkeypatch,m,sigma,kappa,one_minus_phi,dt):

    options = {
               'SimulationDir':'None',
               "SimulationLabel":'None',
               "NumberOfSimulations":1,
               "NumberOfPoints":m,
               "SamplingPeriod":dt,
               "TimeNoiseStart":0,
               "GGM_1mphi":one_minus_phi,
               'NoiseModels': ["GGM"],
               'RepeatableNoise': True
                }

    inputs = [kappa,sigma]
    inputs = '\n'.join(map('{:.3f}'.format,inputs)) + '\n'
    # Kappa, Sigma

    stdin_call = lambda x: monkeypatch.setattr('sys.stdin',
                                               io.StringIO(x))

    myio, mocks = call_stable_main(options,inputs,stdin_call)

    noise = pd.read_fwf(myio,comment='#',names=['noise'],index_col=0)

    myio.close()

    #LOGGER.info(noise)

    y = hps.create_noise(m,dt,0,[('GGM',m,sigma,kappa,one_minus_phi,'mom',dt)],rng=np.random.default_rng(0))

    # LOGGER.info(f"nuevo: {len(y)}, viejo: {len(noise)}")
    # LOGGER.info(cm.mock_calls)
    # LOGGER.info(mparams.mock_calls)

    differ = (y - noise.noise.values)
    assert all(differ < 1e-5)

    return None



from itertools import repeat, chain

NROUND = 5

data = []

model = "Powerlaw"
sigma = np.random.uniform(0,10,N)
kappa = np.random.uniform(-2,2,N)
m = np.random.randint(10,1000,N)
dt = np.random.uniform(0.5,20,N)

sigma, kappa, dt = map(lambda x: np.round(x,NROUND),(sigma,kappa,dt))

cli_params = zip(kappa,sigma)
model_params = zip(repeat(model),m,sigma,kappa,repeat('mom'),dt)

data.append( zip(repeat(model),m,dt,cli_params,model_params) )

model = "Flicker"

cli_params = zip(sigma)
model_params = zip(repeat(model),m,sigma,repeat('mom'),dt)

data.append( zip(repeat(model),m,dt,cli_params,model_params) )

model = "RandomWalk" #Shares parameters with Flicker

cli_params = zip(sigma)
model_params = zip(repeat(model),m,sigma,repeat('mom'),dt)

data.append( zip(repeat(model),m,dt,cli_params,model_params) )

model = "GGM"

one_minus_phi = np.random.uniform(1e-5,1,N)
one_minus_phi = np.round(one_minus_phi,NROUND)

cli_params = zip(one_minus_phi,kappa,sigma)
model_params = zip(repeat(model),m,sigma,kappa,one_minus_phi,repeat('mom'),dt)

data.append( zip(repeat(model),m,dt,cli_params,model_params) )

model = "VaryingAnnual"
phi = np.random.uniform(1e-5,1,N)
phi = np.round(phi,NROUND)

cli_params = zip(phi,sigma)
model_params = zip(repeat(model),m,sigma,phi,repeat('mom'),dt)

data.append( zip(repeat(model),m,dt,cli_params,model_params) )

model = "Matern"

lamba = np.random.uniform(1,10,N) * 10 ** np.random.uniform(-3,-1,N)
kappa = np.random.uniform(-2,-0.5,N) # Kappa for Matern differs from other models

lamba, kappa = map(lambda x: np.round(x,NROUND),(lamba,kappa))

cli_params = zip(lamba,kappa,sigma)
model_params = zip(repeat(model),m,sigma,lamba,kappa)

data.append( zip(repeat(model),m,dt,cli_params,model_params) )

model = "AR1"

cli_params = zip(phi,sigma)
model_params = zip(repeat(model),m,sigma,phi)

data.append( zip(repeat(model),m,dt,cli_params,model_params) )


testdata=chain(*data)

@pytest.mark.parametrize("model,m,dt,cli_params,model_params",testdata)
def test_noise_compare_generic(monkeypatch,model,m,dt,cli_params,model_params):

    options = {
               'SimulationDir':'None',
               "SimulationLabel":'None',
               "NumberOfSimulations":1,
               "NumberOfPoints":m,
               "SamplingPeriod":dt,
               "TimeNoiseStart":0,
               'NoiseModels': [model],
               'RepeatableNoise': True
                }

    # --- manual imput parameters ---
    inputs = '\n'.join(map(('{:.'+f'{NROUND}'+'f}').format,cli_params)) + '\n'

    stdin_call = lambda x: monkeypatch.setattr('sys.stdin',
                                               io.StringIO(x))

    myio, mocks = call_stable_main(options,inputs,stdin_call)

    noise = pd.read_fwf(myio,comment='#',names=['noise'],index_col=0)

    myio.close()

    #LOGGER.info(noise)

    y = hps.create_noise(m,dt,0,[model_params],rng=np.random.default_rng(0))

    # LOGGER.info(f"nuevo: {len(y)}, viejo: {len(noise)}")
    # LOGGER.info(cm.mock_calls)
    # LOGGER.info(mparams.mock_calls)

    differ = (y - noise.noise.values)
    assert all(differ < 1e-5)

    return None
