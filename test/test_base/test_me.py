import hectorpp.mock_estimatrend as me
import tensorflow as tf
import pandas as pd

def test_anda():
    ds = tf.data.Dataset.load("/home/javier/proy/dafne.pub/test/paperRev/dsynth_soft/validation/")
    a,*_ = ds.batch(100000).take(1)
    data = pd.DataFrame(a[0][1][:,1],columns=['obs'])

    me._estimate_trend(data,NoiseModels=['White','Powerlaw'],
                    postesismicexp = [],
                    postesismiclog = [],
                    ssetanh = [],
                    useRMLE = False,
                    periodicsignals = [],
                    sampling_period = 1,
                    offsets=[31] ,
                    myio=None)
