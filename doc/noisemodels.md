# Noise models

HectorP uses a noise model to construct the covariance matrix used in the weighted least-squares estimation. That is, each noise model has a covariance function associated to it. The parameters in this function are estimated using Maximum Likelihood Estimation. 

Geophysical time series are almost always temporally correlated. That is, two measurements taken only a short time apart will be similar. Think of the weather, if it is hot today, changes are high that it will be hot tomorrow. One of the most famous examples of this are the Nile records which were analysed by Hurst. A nice historical summary is given by [Sutcliffe et al. (2016)](https://www.tandfonline.com/doi/pdf/10.1080/02626667.2015.1019508). Another good article about this topic is the one by [Press (1978)](https://www.lanl.gov/dldstp/Flicker_Noise_1978.pdf). 

If you throw a dice, the dice has no memory of what happened before and as a result the temporal correlation is zero. This is also called white noise. A drunken man walking home will at each step deviate a bit to the left or right. Each deviation adds to all deviations he made before. This type of behaviour is called 'random walk'. 

One of the most famous examples of random walk is the motion of a pollen in water that was observed by Brown under a microscope [(Brownian motion)](https://en.wikipedia.org/wiki/Brownian_motion). Einstein modelled this as the result of bombardment of water molecules which move the pollen randomly each step to a new position. Below are some examples of power-law noise.

![Examples of power-law noise](figures/powerlaw_noise.png)

The power spectral density plot of these three time series is plotted below:

![Power spectral density plots of power-law noise](figures/PSD.png)

The slope of the power spectral density is called the 'spectral index'. In GNSS research this is represented by the variable $`\kappa`$. 

## White Noise

White noise is defined as a stocastich process for which the power spectral density is constant for all frequencies. In the time domain it means that each measurement is uncorrelated with the other measurements (zero correlation). If we assume that the standard deviation of the noise is $`\sigma_w`$, then the covariance function is:
```math
\begin{split}
 \gamma({\tau}) = \gamma_i &= \sigma_w^2 \;\;\;\text{for }\tau=0\\
              &=0   \;\;\;\text{for }\tau>0
\end{split}
```

where $`\tau`$ is the delay between two measurements. With sampling period $`\Delta T`$, this can be written as $`\tau=i\Delta T`$ with $`i`$ the number of points between the observations. Its one-sided power spectrum density is:
```math
 S(f) = 2\frac{\sigma_w^2}{f_s}
```

where $`f_s`$ is the sampling frequency in Hz. If you integrate
this from zero frequency to the Nyquist frequency, you get the variance
that is observed in the time series, as it should be.


## Power-law noise


For power-law noise the first column of the covariance matrix is, assuming that the noise has a standard deviation of $`\sigma_{pl}`$:
```math
\gamma_i = \sigma_{pl}^2\frac{\Gamma(d+i)\Gamma(1-2d)}
        {\Gamma(d)\Gamma(1+i-d)\Gamma(1-d)}
```  

$`\Gamma`$ is the gamma function. The parameter $`d`$ is related to the spectral index $`\kappa`$ as follows: $`\kappa=-2d`$. The parameter $`d`$ is used in econometric papers. Its valuse should be between -1 to 0.5 in order to generate stationary noise.

Note that the noise amplitude $`\sigma_{pl}`$ has the unit of $`mm/yr^{-\kappa/4}`$. Of course if another signal than GNSS is investigated the unit of mm will be different. What we like to point out here is that the scaling of $`yr^{-\kappa/4}`$ helps to compute a noise amplitude for different sampling periods. For example, for daily observations, the noise amplitude that is used to create the covariance matrix gets scaled as follows:
```math
\sigma_{daily} = \sigma_{pl} (1/365.25)^{-\kappa/4}
```

For monthly data we get:
```math
\sigma_{monthly} = \sigma_{pl} (1/12)^{-\kappa/4}
```

Its one-sided power spectrum density, with $`\sigma`$ [=$`\sigma_{daily}`$ or $`\sigma_{monthly}`$] is:
```math
 S(f) = 2\frac{\sigma^2}{f_s}\frac{1}{(2\sin(\pi f/f_s))^{2d}}
```

## Generalised Gauss Markov

[John Langbein (2004)](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2003JB002819) took the first order Gauss Markov noise model depending
on the parameter $`\phi`$ and
modified it with an additional parameter, $`d`$, to create power-law noise with
a slope of $`\kappa`$ in the power density spectrum which flattens to white
noise at the very low and very high frequencies. The analytical expression
for the autocovariance vector (with $`\sigma_{pl}`$) for this noise model is:
```math
  \gamma_i 
    = \sigma_{pl}^2\frac{\Gamma(d+i)(1-\phi)^i}
       {\Gamma(d)\Gamma(1+i)}
        \,_2F_1(d,d+i;1+i;(1-\phi)^2)
```

$`\,_2F_1`$ is the hypergeometric function.
This noise model can be used using the name 'GGM' after the
keyword Noisemodels in `estimatetrend.ctl`. 

Its one-sided power spectrum density, with $`\sigma_{pl}`$, is:
```math
2\frac{\sigma_{pl}^2}{f_s}\frac{1}{(4(1-\phi)\sin^2(\pi f/f_s) + \phi^2)^d}
```


The $`1-\phi`$ parameter can be held fixed a priori by adding to the
control-file:
```
GGM_1mphi           XXXX
```

where XXXX is the value you want to give this parameter. If
it is small enough,  6.9e-06 is a good value, then GGM approximates a
pure power-law model, see also next sub-section.

## Flicker noise and Random Walk noise

Flicker noise and Random walk noise are simply two types of power-law noise
where the spectral index $`d`$ has the fixed value  of 0.5 and 1.0 respectively ($`\kappa=-1`$ and $`\kappa=-2`$).
However, as was noted in the [main README](../README.md), Hector can only deal with
stationary noise because that results in Toeplitz covariance matrices that
allow fast inversion techniques. This can be achieved
by using the Generalised Gauss Markov noise model with a small value
for the $`1-\phi`$ parameter as was mentioned before. Example:
```
NoiseModels         FlickerGGM or RandomWalkGGM
GGM_1mphi           6.9e-06
```

The value of $`6.9\times10^{-6}`$ looks strange to many people. It is simply
the smallest value that did not give numerical problems in previous versions of HectorP. 

In the past, people have analysed time series that caused Hector (C++ version) to use
values of $`d`$ (i.e. $`-\kappa/2`$) and $`\phi`$ outside the permitatable
limits. In HectorP the `mpmath` module to compute the hypergeometric function seems more stable.

