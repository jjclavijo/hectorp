import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="hectorpp",
    version="0.1.2",
    author="Javier Clavijo over Machiel Bos's work",
    author_email="jclavijo@fi.uba.ar",
    description="A collection of programs to analyse geodetic time series with some modifications form J Clavijo",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jjclavijo/hectorpp",
    project_urls={
        "Bug Tracker": "https://gitlab.com/jjclavjo/hectorpp/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    install_requires=[
        'pandas',
        'numpy',
        'matplotlib',
        'scipy',
        'mpmath'
    ],
    entry_points ={
        'console_scripts': [
            'estimatespectrum = hectorp.estimatespectrum:main',
            'modelspectrum = hectorp.modelspectrum:main',
            'estimatetrend = hectorp.estimatetrend:main',
            'estimate_all_trends = hectorp.estimate_all_trends:main',
            'removeoutliers = hectorp.removeoutliers:main',
            'findoffsets = hectorp.findoffsets:main',
            'simulatenoise = hectorp.simulatenoise:main',
            'mjd2date = hectorp.mjd2date:main',
            'date2mjd = hectorp.date2mjd:main',
            'convert_rlrdata2mom = hectorp.convert_rlrdata2mom:main',
            'predict_error = hectorp.predict_error:main',
            'test_Schur = hectorp.test_Schur:main',
        ],
    }
)
