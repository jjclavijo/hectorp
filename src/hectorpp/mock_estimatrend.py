import unittest.mock as mock
import pandas as pd
import numpy as np

# a = get_ipython().configurables[0]; a.editing_mode='vi'; a.init_prompt_toolkit_cli()

# Individual import for each module that needs to be patched.
import hectorpp.observations
import hectorpp.estimatetrend
import hectorpp.designmatrix
import hectorpp.mle
import hectorpp.covariance
import hectorpp.powerlaw
import hectorpp.white
import hectorpp.ammargrag

# import SingletonMeta to have SingletonMeta.clear_all()
from hectorpp.control import SingletonMeta

# For patching sys.argv and json.dump
import sys
import json

# For redirecting output of ammargrag to log.
import logging
aglogger = logging.getLogger('ammargrag')


def _get_control_mock(**kwargs):
    control = kwargs
    mparams = mock.Mock(side_effect = control.__getitem__)
    cm = mock.MagicMock(**{"params.__getitem__":mparams})
    cm.return_value = cm

    # Return the object to patch, and the gettitem mock to monitor
    # options access.

    return cm, mparams

_model_defaults = {
      'white': (gm := dict(Verbose = False, PhysicalUnit = 'm') ),
      'powerlaw': gm,
      'ar1': gm, #admits phi_ar1_fixed
      'ggm': dict(GGM_1mphi = 1e-6,**gm), #admits kappa_fixed
      'matern': gm, #admits lambda_fixed, #admits kappa_fixed
      'varyingannual': dict(TS_format='mom',**gm) #admits phi_varying_fixed
      }

def _get_Model_mock(model,**kwargs):
    params = {**_model_defaults[model.lower()],**kwargs}
    return _get_control_mock(**params)

class ModelPatch():
    def __init__(self,NoiseModels):
        self.patch=[]
        self.patched=[]
        self.pmocks = {}
        if isinstance(NoiseModels,list):
            for model in NoiseModels:
                cmock,pmock = _get_Model_mock(model)
                self.patch.append(
                        mock.patch('hectorpp.' + model.lower() + '.Control',cmock)
                        )
                self.pmocks[model] = pmock

        if isinstance(NoiseModels,dict):
            for model,p in NoiseModels.items():
                cmock,pmock = _get_Model_mock(model,**p)
                self.patch.append(
                        mock.patch('hectorpp.' + model.lower() + '.Control',cmock)
                        )
                self.pmocks[model] = pmock

    def __enter__(self):
        for patch in self.patch:
            try:
                self.patched.append(patch.__enter__())
            except Exception as e:
                raise
        return self

    def __exit__(self,*exec_info):
        for patch in self.patched:
            patch.__exit__()

def _estimate_trend(data,NoiseModels=['White','Powerlaw'],
                    postseismicexp = [],
                    postseismiclog = [],
                    ssetanh = [],
                    useRMLE = False,
                    periodicsignals = [],
                    sampling_period = 1,
                    degree_polynomial = 1,
                    offsets=[31],
                    myio=None):

    # ----------------------------------------------
    # --- Prepare patch for Observations object. ---
    # ----------------------------------------------

    m = len(data.index)
    n = data['obs'].isna().sum()
    F = np.zeros((m,n))
    j=0
    for i in range(0,m):
        if np.isnan(data.iloc[i,0])==True:
            F[i,j]=1.0
            j += 1

    percentage_gaps = 100.0 * n/m

    def data_add_mod(*args,**kwargs):
        array = args[-1]
        data.loc[:,'mod'] = np.array(array).flatten()
        return None

    def show_results(*args,**kwargs):
        """ add info to json-ouput dict
        """
        output = args[-1]

        output['N'] = m
        output['gap_percentage'] = percentage_gaps
        output['TimeUnit'] = 'days'

    writeMock = mock.Mock(return_value=None)

    dictio = {
            'data' : data,
            'ts_format' : "mom",
            'sampling_period' : sampling_period,
            'offsets' : offsets,
            'postseismicexp' : postseismicexp,
            'postseismiclog' : postseismiclog,
            'ssetanh' : ssetanh,
            'm' : m,
            'F' : F,
            'percentage_gaps' : percentage_gaps,
            'add_mod' : data_add_mod,
            'show_results' : show_results,
            'write' : writeMock
    }

    om = mock.MagicMock(**dictio)
    om.return_value = om

    # ----------------------------------------------
    # --- Prepare patch for Control object.      ---
    # ----------------------------------------------
    #
    # The patching is done on each submodule.
    #

    # hectorpp.estimatetrend
    cm_estimatrend,_ = _get_control_mock( Verbose = False, DataFile = '',
                                        PhysicalUnit = 'm', PlotName = '',
                                        OutputFile = '' )

    eslowslipevent = epostseismic = eoffsets = False
    if offsets != []: eoffsets = True
    if (postseismicexp != []) or (postseismiclog != []): epostseismic = True
    if ssetanh != []: eslowslipevent = True

    # hectorpp.designmatrix
    cm_designmatrix,_ = _get_control_mock( Verbose = False,
                        periodicsignals = periodicsignals, PhysicalUnit = 'm',
                        seasonalsignal = False, halfseasonalsignal = False,
                        DegreePolynomial = degree_polynomial, estimateoffsets = eoffsets,
                        estimatepostseismic = epostseismic,
                        estimateslowslipevent = eslowslipevent)

    # hectorpp.mle
    cm_mle,_ = _get_control_mock( Verbose = False, useRMLE = useRMLE )

    # hectorpp.covariance

    tmp_params = {}
    if isinstance(NoiseModels,dict):
        tmp_params['NoiseModels'] = [*NoiseModels.keys()]
        # TODO: Handle kappa_fixed parameter, that affects covariance submodule
        for k,v in NoiseModels.items(): #Iterate, so we can ignore Case
            if k.lower() == 'ggm' and 'kappa_fixed' in v:
                tmp_params['kappa_fixed'] = v['kappa_fixed']
            if k.lower() == 'matern' and 'kappa_fixed' in v:
                tmp_params['kappa_fixed'] = v['kappa_fixed']
    else:
        tmp_params['NoiseModels'] = NoiseModels

    cm_covariance,_ = _get_control_mock( Verbose = False, **tmp_params)

    """
    # hectorpp.predicttrenderror

    cm_predicttrenderror,_ = _get_control_mock( Verbose = False)
    """

    # ----------------------------------------------------
    # --- Prepare patch for Cli input and file Output. ---
    # ----------------------------------------------------
    #
    # The patching is done on each submodule.
    #

    my_argv = ['estimatetrend']

    mockDump = mock.Mock()

    mockFile = mock.MagicMock(__call__=lambda *x,**y: x[0],__enter__=lambda x:x)
    mockOpen = mock.Mock(return_value=mockFile)

    """
    Control en observations.

    Verbose, ScaleFactor, DataFile, DataDirectory, TS_format, UseResiduals,
    ObservationColumn

    Control en predicttrenderror

    Verbose
    """

    with ModelPatch(NoiseModels),\
         mock.patch('json.dump',mockDump), \
         mock.patch('hectorpp.estimatetrend.open',mockOpen), \
         mock.patch('hectorpp.observations.Observations',om) ,\
         mock.patch('hectorpp.estimatetrend.Observations',om) ,\
         mock.patch('hectorpp.designmatrix.Observations',om) ,\
         mock.patch('hectorpp.mle.Observations',om) ,\
         mock.patch('hectorpp.powerlaw.Observations',om) ,\
         mock.patch('hectorpp.ggm.Observations',om) ,\
         mock.patch('hectorpp.estimatetrend.Control',cm_estimatrend) ,\
         mock.patch('hectorpp.designmatrix.Control',cm_designmatrix) ,\
         mock.patch('hectorpp.mle.Control',cm_mle) ,\
         mock.patch('hectorpp.covariance.Control',cm_covariance) ,\
         mock.patch('hectorpp.ammargrag.print',aglogger.info),\
         mock.patch('sys.argv',my_argv):

        #print("Corriendo Todo")

        try:
            hectorpp.estimatetrend.main()
        except Exception as e:
            #TODO: unittest for exceptions and issue Warnings
            SingletonMeta.clear_all()
            raise
        else:
            SingletonMeta.clear_all()

    optimization_output = mockDump.call_args.args[0]

    return(optimization_output,data)

from scipy import optimize

n_params = [1,1,2,3,3,4]
models = [ lambda T1: dict( postseismicexp = [(31,T1)],
                        postseismiclog = []),
           lambda T1: dict( postseismicexp = [],
                        postseismiclog = [(31,T1)]),
           lambda T1,T2: dict( postseismicexp = [(31,T1)],
                           postseismiclog = [(31,T2)]),
           lambda T1,T2,T3: dict( postseismicexp = [(31,T1),(31,T2)],
                              postseismiclog = [(31,T3)]),
           lambda T1,T2,T3: dict( postseismicexp = [(31,T1)],
                              postseismiclog = [(31,T2),(31,T3)]),
           lambda T1,T2,T3,T4: dict( postseismicexp = [(31,T1),(31,T2)],
                                 postseismiclog = [(31,T3),(31,T4)])
          ]

from numpy.linalg import LinAlgError

def _model_param_starter():
    params = [np.random.rand(n)*10 for n in n_params]
    def _start_values(model):
        return params[model]
    def _update_values(model,result):
        params[model] += (result - params[model]) * 0.1

    return _start_values,_update_values

_sv,_uv = _model_param_starter()

def test_models(datas):

    minimizers = []
    for nm,(n, opts) in enumerate( zip(n_params,models)):

        def fun(x):
            x = np.maximum(np.abs(x),0.0001)

            AIC = 0

            for data in datas:
                out, new_data = _estimate_trend(data,
                                    NoiseModels=['White'],
                                    ssetanh = [],
                                    useRMLE = False,
                                    periodicsignals = [],
                                    sampling_period = 1,
                                    offsets=[31] ,
                                    myio=None,
                                    **opts(*x) )

            AIC += out['AIC']

            return AIC

        error = True
        while error:
            try:
                print(_sv(nm))
                minimizer = optimize.minimize(fun,x0=_sv(nm))
                error = False
            except LinAlgError:
                nuevo = _sv(nm)
                to_up = int(np.random.randint(n))
                nuevo[to_up] = np.random.uniform(10)**2
                _uv(nm,nuevo)
                error = True
                print('1 error')
                pass

        _uv(nm,minimizer.x)
        minimizers.append(minimizer)

    return minimizers

# Otro

n_params = [1,1,1,1,2,2,2,2]
models = [
           lambda T1: dict( postseismicexp = [(31,T1)],
                        postseismiclog = []),
           lambda T1: dict( postseismicexp = [(31,T1)],
                        postseismiclog = []),
           lambda T1: dict( postseismicexp = [],
                        postseismiclog = [(31,T1)]),
           lambda T1: dict( postseismicexp = [],
                        postseismiclog = [(31,T1)]),
           lambda T1,T2: dict( postseismicexp = [(31,T1)],
                           postseismiclog = [(31,T2)]),
           lambda T1,T2: dict( postseismicexp = [(31,T1)],
                           postseismiclog = [(31,T2)]),
           lambda T1,T2: dict( postseismicexp = [(31,T1)],
                           postseismiclog = [(31,T2)]),
           lambda T1,T2: dict( postseismicexp = [(31,T1)],
                           postseismiclog = [(31,T2)])
          ]

def _model_param_starter():
    params = [
            np.array([10]).astype(float),
            np.array([100]).astype(float),
            np.array([10]).astype(float),
            np.array([100]).astype(float),
            np.array([10,20]).astype(float),
            np.array([90,100]).astype(float),
            np.array([100,10]).astype(float),
            np.array([10,100]).astype(float),
             ]
    def _start_values(model):
        return params[model].copy()
    def _update_values(model,result):
        params[model] += (result - params[model]) * 0.1
        pass

    return _start_values,_update_values

_sv,_uv = _model_param_starter()

def test_models(datas):

    minimizers = []
    for nm,(n, opts) in enumerate( zip(n_params,models)):

        def fun(x):
            x = np.abs(x)+3

            AIC = 0

            for data in datas:
                out, new_data = _estimate_trend(data,
                                    NoiseModels=['White'],
                                    ssetanh = [],
                                    useRMLE = False,
                                    periodicsignals = [],
                                    sampling_period = 1,
                                    offsets=[31] ,
                                    myio=None,
                                    **opts(*x) )

            AIC += out['AIC']

            return AIC

        error = True
        while error:
            try:
                print(_sv(nm))
                minimizer = optimize.minimize(fun,x0=_sv(nm))
                error = False
            except LinAlgError:
                nuevo = _sv(nm)
                to_up = int(np.random.randint(n))
                nuevo[to_up] = np.random.uniform(10)
                _uv(nm,nuevo)
                error = True
                print('1 error')
                pass

        #_uv(nm,minimizer.x)
        minimizers.append(minimizer)

    return minimizers

n_params = [0,1,1,1,1]
models = [
           lambda : dict( ),
           lambda T1: dict( postseismicexp = [(31,T1)],
                        postseismiclog = []),
           lambda T1: dict( postseismicexp = [(31,T1)],
                        postseismiclog = []),
           lambda T1: dict( postseismicexp = [],
                        postseismiclog = [(31,T1)]),
           lambda T1: dict( postseismicexp = [],
                        postseismiclog = [(31,T1)]),
          ]

def _model_param_starter():
    params = [
            np.array([]).astype(float),
            np.array([10]).astype(float),
            np.array([200]).astype(float),
            np.array([10]).astype(float),
            np.array([200]).astype(float),
             ]
    def _start_values(model):
        return params[model].copy()
    def _update_values(model,result):
        params[model] += (result - params[model]) * 0.1
        pass

    return _start_values,_update_values

_sv,_uv = _model_param_starter()

from types import SimpleNamespace

def test_models(datas):

    minimizers = []
    for nm,(n, opts) in enumerate( zip(n_params,models)):

        def fun(x):
            x = np.abs(x)+2

            AIC = 0

            for data in datas:
                out, new_data = _estimate_trend(data,
                                    NoiseModels=['White'],
                                    ssetanh = [],
                                    useRMLE = False,
                                    periodicsignals = [],
                                    sampling_period = 1,
                                    offsets=[31] ,
                                    myio=None,
                                    **opts(*x) )

            AIC += out['AIC']

            return AIC

        if n == 0:
            minimizer = SimpleNamespace()
            minimizer.x = []
            minimizer.fun = fun([])
            minimizers.append(minimizer)
            continue

        error = True
        while error:
            try:
                print(_sv(nm))
                minimizer = optimize.minimize(fun,x0=_sv(nm))
                error = False
            except LinAlgError:
                nuevo = _sv(nm)
                to_up = int(np.random.randint(n))
                nuevo[to_up] = np.random.uniform(10)
                _uv(nm,nuevo)
                error = True
                print('1 error')
                pass

        #_uv(nm,minimizer.x)
        minimizers.append(minimizer)

    return minimizers
