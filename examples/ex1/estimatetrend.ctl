DataFile            TEST.mom
DataDirectory       pre_files
OutputFile          fin_files/TEST.mom
interpolate         no
PhysicalUnit        km^3
ScaleFactor         1.0
periodicsignals     365.25 182.625
estimateoffsets     yes
NoiseModels          GGM White
GGM_1mphi           6.9e-06
useRMLE             no
Verbose             yes 
