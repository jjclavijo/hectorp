DataFile              TEST.mom
DataDirectory         obs_files
OutputFile            pre_files/TEST.mom
periodicsignals       365.25 182.625
estimateoffsets       yes
estimatepostseismic   yes
estimateslowslipevent yes
ScaleFactor           1.0
PhysicalUnit          mm
IQ_factor             3
Verbose               no
