DataFile              TEST.mom
DataDirectory         ./obs_files
OutputFile            ./fin_files/TEST.mom
estimateoffsets       yes
estimatepostseismic   yes
#NoiseModels           GGM White
NoiseModels           White
GGM_1mphi             6.9e-06
PhysicalUnit          mm
ScaleFactor           1.0
TimeNoiseStart        1000.0
