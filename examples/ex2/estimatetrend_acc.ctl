DataFile            test_data.msf
DataDirectory       pre_files
OutputFile          fin_files/step1.msf
PhysicalUnit        mGal
ScaleFactor         1.0
NoiseModels         GGM White
GGM_1mphi           1.0e-05
useRMLE             no
TS_format           msf
ObservationColumn   3
PlotName            accelerometer
