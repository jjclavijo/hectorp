DataFile              test_data.msf
DataDirectory         fin_files
interpolate           no
ScaleFactor           1.0
PhysicalUnit          mGal
Verbose               no
TS_format             msf
ObservationColumn     3
UseResiduals          yes
PlotName              accelerometer
