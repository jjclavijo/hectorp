DataFile            step1.msf
DataDirectory       fin_files
OutputFile          fin_files/test_data.msf
PhysicalUnit        º/s
ScaleFactor         1.0
NoiseModels         Powerlaw White
GGM_1mphi           1.0e-05
useRMLE             no
TS_format           msf
ObservationColumn   4
PlotName            gyroscope
